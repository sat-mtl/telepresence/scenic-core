#!/bin/bash

# exit on error
set -e

# Install ndi2shmdata dependencies
apt-get install -y -qq --no-install-recommends \
    libavahi-client-dev
