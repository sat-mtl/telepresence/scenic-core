#!/bin/bash

# exit on error
set -e

cd /opt
git clone --single-branch --branch ${CERBERO_TAG} https://gitlab.freedesktop.org/gstreamer/cerbero
cd cerbero
./cerbero-uninstalled bootstrap --assume-yes
