#!/bin/bash

# exit on error
set -e

# Log that we are running in custom GStreamer SDK
echo "Using GSTREAMER_ROOT=$GSTREAMER_ROOT"

# Build Shmdata
cd /opt/shmdata
cmake -GNinja -B build \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo
ninja -C build
ninja install -C build
ldconfig
