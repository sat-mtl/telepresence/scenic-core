#!/bin/bash

# exit on error
set -e

# Fetch Switcher sources
cd /opt
git clone -b $SWITCHER_TAG https://gitlab.com/sat-mtl/tools/switcher.git
cd switcher
git submodule update --init --recursive
