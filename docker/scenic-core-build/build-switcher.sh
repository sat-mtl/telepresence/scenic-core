#!/bin/bash

# exit on error
set -e

# Log that we are running in custom GStreamer SDK
echo "Using GSTREAMER_ROOT=$GSTREAMER_ROOT"

# Build Switcher
cd /opt/switcher
cmake -GNinja -B build \
    -DENABLE_GPL=ON \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DPLUGIN_VRPN=OFF \
    -DPLUGIN_VNC=OFF \
    -DPLUGIN_PROTOCOL_MAPPER=OFF \
    -DPLUGIN_TIMECODE=OFF \
    -DPLUGIN_WEBRTC=OFF
ninja -C build
ninja install -C build
ldconfig
