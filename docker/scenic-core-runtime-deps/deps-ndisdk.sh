#!/bin/bash

# exit on error
set -e

# Find architecture path
ARCH=$(dpkg --print-architecture)
if [[ $ARCH == "amd64" ]]; then 
    LIBNDI_ARCH_PATH_PREFIX="x86_64-linux-gnu"
fi
if [[ $ARCH == "arm64" ]]; then 
    LIBNDI_ARCH_PATH_PREFIX="aarch64-rpi4-linux-gnueabi"
fi
if [[ $ARCH == "armhf" ]]; then 
    LIBNDI_ARCH_PATH_PREFIX="arm-rpi1-linux-gnueabihf"
fi

echo installing $LIBNDI_ARCH_PATH_PREFIX architecture

# Install ndisdk from redistributable
LIBNDI_INSTALLER_NAME="Install_NDI_SDK_v5_Linux"
LIBNDI_INSTALLER="$LIBNDI_INSTALLER_NAME.tar.gz"
pushd /tmp
apt-get install -y -qq --no-install-recommends curl ca-certificates
curl -L -o $LIBNDI_INSTALLER https://downloads.ndi.tv/SDK/NDI_SDK_Linux/$LIBNDI_INSTALLER_NAME.tar.gz -f --retry 5
sha256sum $LIBNDI_INSTALLER
tar -xf $LIBNDI_INSTALLER
yes | PAGER="cat" sh $LIBNDI_INSTALLER_NAME.sh
rm -rf ndisdk
mv "NDI SDK for Linux" ndisdk
cp -P ndisdk/lib/$LIBNDI_ARCH_PATH_PREFIX/* /usr/local/lib/
cp -P ndisdk/bin/$LIBNDI_ARCH_PATH_PREFIX/* /usr/local/bin/
ldconfig
echo libndi installed to /usr/local/lib/
ls -la /usr/local/lib/libndi*
echo ndi utilities installed to /usr/local/bin/
ls -la /usr/local/bin/ndi-*
rm -rf ndisdk
popd