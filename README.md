# Scenic Core

![Scenic Logo](/img/scenic-signature-horizontal.png)

Scenic is a stage teleconference system that allows for real-time transmission of audiovisual and arbitrary data over any IP network. Telepresence systems can be used in various artistic contexts, so that two different creative spaces can communicate with each other or present a combined performance. Scenic Core is a rewrite and improvement of the previous Scenic software.

**SwitcherIO** is a web server which can be used with a compatible user interface using [Socket.io](http://socket.io/) commands. The **Scenic graphical user interface** can be downloaded separately from its [source](https://gitlab.com/sat-mtl/tools/scenic/scenic). As part of the Scenic stack, SwitcherIO acts as a bridge and gatekeeper between [Switcher](https://gitlab.com/sat-mtl/tools/switcher) (telepresence engine) and [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic).

SwitcherIO is currently developed by the [Société des Arts Technologiques [SAT]](http://sat.qc.ca/), a non-profit artistic entity based in Montreal, Canada. The current version of Scenic Core has been tested with **Ubuntu 20.04**.

## Getting Started

To setup Scenic using Docker, follow the instructions in the [Docker usage page](docs/docker-usage.md).

## Development

Development information on Docker images build system is available on the [Docker build page](doc/docker-build.md)

## Technologies

Scenic Core is a custom Docker build system to compile a complete Scenic backend container image. It includes Shmdata, ndi2shmdata, Switcher and SwitcherIO.

It uses a [SwitcherIO](https://gitlab.com/sat-mtl/tools/switcher/-/tree/master/wrappers/switcherio) to communicate with [Switcher](https://github.com/sat-mtl/tools/switcher) and [Socket.io](http://socket.io/) protocol to communicate with [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic).

The SAT uses **[Ubuntu 20.04](http://releases.ubuntu.com/20.04/)**, so testing priority is given to this version.

## Contributing

Check out our [Contributing Guide](CONTRIBUTING.md) to get started!

## Versioning

[SemVer](http://semver.org/) is used for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/tags).

## Authors

See [here](AUTHORS.md).

## License

This project is licensed under the GNU General Public License version 3 - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

This project was made possible by the [Société des Arts Technologiques [SAT]](http://sat.qc.ca/).
