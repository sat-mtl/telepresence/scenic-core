# Scenic-core Docker build system

## Testing nvcodec support

Testing nvenc/nvdec support on a host running Nvidia Container Runtime is possible with following commands. Host needs `nvidia-driver-5xx` installed.

The installation instructions for the Nvidia Container Toolkit are here : https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html

```bash
docker run \
  --init \
  --rm \
  -it \
  --name nvcodec_test_1 \
  --network host \
  --gpus=all \
  --env DISPLAY=:0 \
  --env 'NVIDIA_DRIVER_CAPABILITIES=all' \
  --entrypoint /bin/bash \
  registry.gitlab.com/sat-mtl/tools/scenic/scenic-core/gstreamer:latest
```

Enter GStreamer SDK shell.

```
/opt/gstreamer-scenic-devel/bin/gst-shell
```

Verify that `nvh264enc` and `nvh264dec` elements are detected at runtime by libgstnvcodec.

```
gst-inspect-1.0 nvcodec
```

Execute following command to test nvenc and nvenc.

```
gst-launch-1.0 \
    videotestsrc \
        is-live=true \
    ! "video/x-raw, width=1920, height=1080, framerate=60/1" \
    ! cudaupload \
    ! nvcudah264enc \
        bitrate=2000 \
        aud=false \
        b-frames=0 \
        gop-size=30 \
        preset=5 \
        rate-control=3 \
        repeat-sequence-header=true \
        spatial-aq=true \
        temporal-aq=true \
    ! nvh264dec \
    ! cudadownload \
    ! videoconvert \
    ! autovideosink
```

You should see a running videotestrc on screen, compressed using nvenc, uncompressed using nvdec.

Wait for 30 seconds for encoder and decoder utilization to stabilize then verify their load using following command.

```
nvidia-smi -q -d UTILIZATION
```

You should see average encoding/decoding loads:

```
    ENC Utilization Samples
        Max                               : 21 %
        Min                               : 20 %
        Avg                               : 20 %
    DEC Utilization Samples
        Max                               : 19 %
        Min                               : 17 %
        Avg                               : 18 %
```

Execute following command to test action video.

```
VIDEO="http://scenicos-release.s3-website.ca-central-1.amazonaws.com/video-tests/never-gonna-1080p30-9mbps.mp4"
gst-launch-1.0 \
    uridecodebin \
        uri=$VIDEO \
    ! tee name=t \
    t. \
      ! queue \
      ! autovideosink \
    t. \
      ! queue \
      ! videoconvert \
      ! nvcudah264enc \
          bitrate=2000 \
          aud=false \
          b-frames=0 \
          gop-size=30 \
          preset=5 \
          rate-control=3 \
          repeat-sequence-header=true \
          spatial-aq=true \
          temporal-aq=true \
      ! nvh264dec \
      ! cudadownload \
      ! videoconvert \
      ! autovideosink
```

You should see video displayed two times on screen, first window displays uncompressed original, second window displays compressed using nvenc then uncompressed using nvdec.

```
$ nvidia-smi -q -d UTILIZATION
    ENC Utilization Samples
        Max                               : 7 %
        Min                               : 7 %
        Avg                               : 7 %
    DEC Utilization Samples
        Max                               : 6 %
        Min                               : 6 %
        Avg                               : 6 %
```

We can observe a encoding/decoding load of about 6-7%.

## Install GStreamer SDK locally

Download gstreamer Docker image and compress SDK in archive in $PWD directory of your workstation.

```
export CERBERO_TAG="latest"
export REGISTRY="registry.gitlab.com/sat-mtl/tools/scenic/scenic-core"
docker run \
  -it \
  --volume $PWD:/out \
  --entrypoint tar \
  $REGISTRY/gstreamer:$CERBERO_TAG \
  -zcf /out/gstreamer-scenic-devel-$CERBERO_TAG.tar.gz -C /opt gstreamer-scenic-devel
```

Extract SDK in /opt/gstreamer-scenic-devel. Remove old SDK before if present.

```
sudo rm -rf /opt/gstreamer-scenic-devel
sudo tar -axvf gstreamer-scenic-devel-$CERBERO_TAG.tar.gz -C /opt
```

Enter SDK shell.

```
/opt/gstreamer-scenic-devel/bin/gst-shell
```

Verify GStreamer version.

```
gst-inspect-1.0 --version
```

Test playing back a video.

```
VIDEO="http://scenicos-release.s3-website.ca-central-1.amazonaws.com/video-tests/never-gonna-1080p30-9mbps.mp4"
gst-launch-1.0 \
    uridecodebin \
        uri=$VIDEO \
    ! autovideosink
```

## Bootstrap multiarch buildx builder

```bash
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --use --name mybuilder
docker login registry.gitlab.com
``````

## Build and push gstreamer image

```bash
export PLATFORMS="linux/amd64"
export CERBERO_TAG="1.24.2"
export DATE=$(date +%Y%m%d)
export REGISTRY="registry.gitlab.com/sat-mtl/tools/scenic/scenic-core"
docker buildx build \
  --progress plain \
  --provenance false \
  --push \
  --platform $PLATFORMS \
  -t $REGISTRY/gstreamer:$CERBERO_TAG-$DATE \
  -f docker/gstreamer/Dockerfile \
  --build-arg "CERBERO_TAG=$CERBERO_TAG" \
.

docker pull $REGISTRY/gstreamer:$CERBERO_TAG-$DATE

docker tag \
  $REGISTRY/gstreamer:$CERBERO_TAG-$DATE \
  $REGISTRY/gstreamer:latest

docker push $REGISTRY/gstreamer:latest
```

## Build and push scenic-core-build-deps image

```bash
export PLATFORMS="linux/amd64"
export REGISTRY="registry.gitlab.com/sat-mtl/tools/scenic/scenic-core"
export BUILD_DEPS_TAG="22.04"
export DATE=$(date +%Y%m%d)
docker buildx build \
  --progress plain \
  --provenance false \
  --push \
  --platform $PLATFORMS \
  -t $REGISTRY/scenic-core-build-deps:$BUILD_DEPS_TAG-$DATE \
  -f docker/scenic-core-build-deps/Dockerfile \
.

docker pull $REGISTRY/scenic-core-build-deps:$BUILD_DEPS_TAG-$DATE

docker tag \
  $REGISTRY/scenic-core-build-deps:$BUILD_DEPS_TAG-$DATE \
  $REGISTRY/scenic-core-build-deps:latest

docker push $REGISTRY/scenic-core-build-deps:latest
```

## Build and push scenic-core-runtime-deps image

```bash
export PLATFORMS="linux/amd64"
export REGISTRY="registry.gitlab.com/sat-mtl/tools/scenic/scenic-core"
export RUNTIME_DEPS_TAG="22.04"
export DATE=$(date +%Y%m%d)
docker buildx build \
  --progress plain \
  --provenance false \
  --push \
  --platform $PLATFORMS \
  -t $REGISTRY/scenic-core-runtime-deps:$RUNTIME_DEPS_TAG-$DATE \
  -f docker/scenic-core-runtime-deps/Dockerfile \
.

docker pull $REGISTRY/scenic-core-runtime-deps:$RUNTIME_DEPS_TAG-$DATE

docker tag \
  $REGISTRY/scenic-core-runtime-deps:$RUNTIME_DEPS_TAG-$DATE \
  $REGISTRY/scenic-core-runtime-deps:latest

docker push $REGISTRY/scenic-core-runtime-deps:latest
```

## Build and push scenic-core-build image

```bash
export PLATFORMS="linux/amd64"
export REGISTRY="registry.gitlab.com/sat-mtl/tools/scenic/scenic-core"
export CERBERO_TAG="latest"
export SCENIC_CORE_BUILD_DEPS_TAG="latest"
export SCENIC_CORE_BUILD_TAG=<your-current-working-branch>
export SHMDATA_TAG="1.3.66"
export NDI2SHMDATA_TAG="candidate/0.7.0"
export SWITCHER_TAG="develop"
export GST_PLUGINS_RS_GIT_URL="https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs.git"
export GST_PLUGINS_RS_TAG="main"
export GST_PLUGINS_RS_GIT_COMMIT="caa1451f"
export SWITCHER_PIPESPLINT_TAG="develop"
docker buildx build \
  --progress plain \
  --provenance false \
  --push \
  --platform $PLATFORMS \
  -t $REGISTRY/scenic-core-build:$SCENIC_CORE_BUILD_TAG \
  -f docker/scenic-core-build/Dockerfile \
  --build-arg "CERBERO_TAG=$CERBERO_TAG" \
  --build-arg "SCENIC_CORE_BUILD_DEPS_TAG=$SCENIC_CORE_BUILD_DEPS_TAG" \
  --build-arg "SHMDATA_TAG=$SHMDATA_TAG" \
  --build-arg "NDI2SHMDATA_TAG=$NDI2SHMDATA_TAG" \
  --build-arg "SWITCHER_TAG=$SWITCHER_TAG" \
  --build-arg "GST_PLUGINS_RS_GIT_URL=$GST_PLUGINS_RS_GIT_URL" \
  --build-arg "GST_PLUGINS_RS_TAG=$GST_PLUGINS_RS_TAG" \
  --build-arg "GST_PLUGINS_RS_GIT_COMMIT=$GST_PLUGINS_RS_GIT_COMMIT" \
  --build-arg "SWITCHER_PIPESPLINT_TAG=$SWITCHER_PIPESPLINT_TAG" \
.
```

## Build and push scenic-core final image

```bash
export PLATFORMS="linux/amd64"
export REGISTRY="registry.gitlab.com/sat-mtl/tools/scenic/scenic-core"
export CERBERO_TAG="latest"
export SCENIC_CORE_RUNTIME_DEPS_TAG="latest"
export SCENIC_CORE_BUILD_TAG=<your-current-working-branch>
export SCENIC_CORE_TAG=<your-current-working-branch>
docker buildx build \
  --progress plain \
  --provenance false \
  --push \
  --platform $PLATFORMS \
  -t $REGISTRY:$SCENIC_CORE_TAG \
  -f docker/scenic-core/Dockerfile \
  --build-arg "CERBERO_TAG=$CERBERO_TAG" \
  --build-arg "SCENIC_CORE_BUILD_TAG=$SCENIC_CORE_BUILD_TAG" \
  --build-arg "SCENIC_CORE_RUNTIME_DEPS_TAG=$SCENIC_CORE_RUNTIME_DEPS_TAG" \
.
```

## Launching scenic-core

The following command launches `scenic-core` from the repo's registry.

It passes hrough any ALSA soundcards, video4linux devices, the X server, and `dbus` access to `avahi-deamon` on the host. It also uses the host's networking and hostname so Docker does not need to NAT Switcher's SIP requests.

Setting DISPLAY variable allows to run Docker run command over SSH and still direct scenic-core GUI output to host's X server.

Host needs a running Jack server; the `scenic-core` Jack client will connect via the shared `/dev/shm` volume.

```bash
docker run \
  --init \
  --rm \
  -it \
  --name scenic-core \
  --network host \
  --security-opt apparmor:unconfined \
  --cap-add=sys_nice \
  --ulimit rtprio=99 \
  --ulimit core=-1 \
  --device-cgroup-rule 'c 81:* rmw' \
  --device '/dev/dri:/dev/dri' \
  --device '/dev/snd:/dev/snd' \
  --volume '/tmp/.X11-unix:/tmp/.X11-unix:ro' \
  --volume '/var/run/dbus:/var/run/dbus' \
  --volume '/dev/shm:/dev/shm:rw' \
  --volume $HOME:/home/scenic \
  --workdir '/home/scenic' \
  --env PUID=`id -u` \
  --env PGID=`id -g` \
  --env PORT=8000 \
  --env DISPLAY=:0 \
  registry.gitlab.com/sat-mtl/tools/scenic/scenic-core:master
```

If running on an `nvidia-container-toolkit` compatible host (`nvidia-docker2` runtime), add those extra parameters to access the GPU and `nvcodec` (nvenc/nvdec) capabilities.

```
  --gpus=all \
  --env 'NVIDIA_DRIVER_CAPABILITIES=all' \
```

If you want to test something inside the scenic-core image, add the following lines to override the entrypoint.

```
  --entrypoint /bin/bash \
```

Here's the reference of the usage for each run arguments.

```
--init                                          # run an init to reap processes
--rm                                            # remove container after use
-it                                             # start in an interactive TTY
--name scenic-core                              # name container
--network host                                  # use host's network
--security-opt apparmor:unconfined              # allow libndi to interact with host's avahi-daemon service
--cap-add=sys_nice                              # allow switcher to re-nice to high priority
--ulimit rtprio=99                              # allow real-time priority
--ulimit core=-1                                # allow core dumps
--device-cgroup-rule 'c 81:* rmw'               # allow access to host's Video4Linux devices
--device '/dev/dri:/dev/dri'                    # allow direct OpenGL rendering on intel/amd/nvidia (dri)
--device '/dev/snd:/dev/snd'                    # allow access to host's midi (snd)
--volume '/tmp/.X11-unix:/tmp/.X11-unix:ro'     # allow access to host's X server
--volume '/var/run/dbus:/var/run/dbus'          # allow access to host's avahi-daemon
--volume '/dev/shm:/dev/shm:rw'                 # allow access to host's Jack server
--volume $HOME:/home/scenic                     # mount Scenic config and logs files into home directory
--workdir '/home/scenic'                        # change directory to home, writes core dumps there
--env PUID=`id -u`                              # map scenic user to local user id
--env PGID=`id -g`                              # map scenic user to local user id
--env DISPLAY=:0                                # tells Scenic to display on host's default X server
--env PORT=8000                                 # listening port for Scenic API
--gpus=all                                      # allow access to Nvidia GPUs
--env 'NVIDIA_DRIVER_CAPABILITIES=all'          # enable Nvidia Container Toolkit
```

## CI pipeline Docker integration

The Scenic Core image is built on 3 base images to modularize and accelerate the final application image creation, so we can get the smallest download size.

Base image `gstreamer` is a standalone GStreamer built using Cerbergo SDK.
Base image `scenic-core-build-deps` contains all third-party source headers and tools needed to compile Switcher.
Base image `scenic-core-runtime-deps` is a smaller image containing only runtime libraries binaries needed to run Switcher.
Intermediate image `scenic-core-build` is based on `scenic-core-build-deps` image and copy GStreamer devel SDK `gstreamer`. It holds Switcher build.
Final image `scenic-core` is based on `scenic-core-runtime-deps`, copies GStreamer runtime from `gstreamer` image, and installs Switcher from `scenic-core-build`. It is the only image needed to deploy the Switcher stack on a Scenic Station.

Base images are built outside of CI on developper workstation, manually tagged with a version and date suffix. Latest builds are also available under `latest` tag.

The CI pipeline automatically builds `scenic-core-build` and `scenic-core` images when running on `develop` branch.

Jobs `release-scenic-core-build-deps-docker-image`, `release-scenic-core-runtime-deps-docker-image` and `release-scenic-core-final-docker-image` are depending on each others and run sequentially thanks to CI dependencies.

Developers can also build any of the above images manually on any branches using jobs `manual-release-scenic-core-build-docker-image-from-branch` and `manual-release-scenic-core-image-from-branch`.

Manual images built using pipeline are automatically uploaded to registry under `$CI_COMMIT_REF_SLUG` name. I.e. if building for a branch named "feat/update-docker-image-build", images will be available under "feat-update-image-build" tag.
